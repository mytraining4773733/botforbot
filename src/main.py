import logging
import random
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.types import ParseMode
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher.storage import FSMContext


TOKEN = "6026322841:AAF9cnKngbCM6ES6mqQIf85XKp_y_lyaruc" 

bot = Bot(token=TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
logging.basicConfig(level=logging.INFO)
dp.middleware.setup(LoggingMiddleware())

class AnketState(StatesGroup):
    wait_name = State()
    wait_gender = State()



# Список вдохновляющих цитат
quotes = [
    "Ты - удивительная женщина, способная на большее, чем ты думаешь.",
    "Не бойся воплощать свои мечты в реальность, потому что ты достойна успеха.",
    "Когда ты веришь в себя, мир тоже начинает верить в тебя.",
    "Сложности — это лишь шанс показать, насколько ты сильна.",
    "Пусть каждый твой день будет наполнен радостью и достижениями.",
    "Ты способна на много больше, чем ты думаешь. Вперед, к своим целям!",
    "Помни, что каждый шаг приближает тебя к твоей мечте.",
    "Ты уникальна и неповторима. Твоя улыбка способна изменить мир вокруг тебя.",
    "Не забывай дарить любовь и заботу себе так же, как ты это делаешь для других.",
    "Помни, что даже в сложные моменты у тебя есть сила и мудрость.",
]




@dp.message_handler(commands="start")
async def start_anket(message: types.Message):
    await message.answer(text="Введите Ваше имя")
    await AnketState.wait_name.set()

@dp.message_handler(state=AnketState.wait_name)
async def get_name(message: types.Message, state: FSMContext):
    print(message.text)
    data_name = await state.get_data()
    data_name ["name"] = message.text
    await state.update_data(data_name)
    await message.answer(text="Введите свой пол (М/Ж)")
    await AnketState.wait_gender.set()

@dp.message_handler(state=AnketState.wait_gender)
async def get_gender(message: types.Message, state: FSMContext):
    print(message.text)
    data_name = await state.get_data()
    data_name ["gender"] = message.text
    await state.update_data(data_name)
    print(data_name)

    if data_name.get("gender") == "м":
        await message.answer(text="Мужчины не плачут")
    elif data_name.get("gender") == "ж":
        quote = random.choice(quotes)
        await message.reply(quote, parse_mode=ParseMode.MARKDOWN)
    else:
        await message.answer(text="Фигню пишешь. Начинай заново!")
    await state.reset_state()


if __name__ == "__main__":
    from aiogram import executor

    executor.start_polling(dp, skip_updates=True)

